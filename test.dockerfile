FROM swift:5.1-bionic
RUN apt -y update
RUN apt install -y sqlite3 libsqlite3-dev
ADD . /src
WORKDIR /src
RUN swift test
